//
//  AppDelegate.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 6/21/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        

        // crear una window
        window = UIWindow(frame:UIScreen.mainScreen().bounds)
        
        // crear instancia de modelo
        do{
            var json  = try loadFromLocalFile(fileName: "regularCharacters.json")
            json.appendContentsOf(try loadFromLocalFile(fileName: "forceSensitives.json"))
            print(json)
            
            
            var chars = [StarWarsCharacter]()
            for dict in json{
                do{
                    let char = try decode(starWarsCharacter: dict)
                    chars.append(char)
                }catch{
                    print("Error al procesar \(dict)")
                }
            
            }
            
            // Podemos crear el modelo
            let model = StarWarsUniverse(characters: chars)
            
            
            // Crear un UniverseVC
            let uVC = UniverseViewController(model: model)
            
            // Lo metemos en un Nav
            let uNav = UINavigationController(rootViewController: uVC)
            
            
            // Creamos un Character VC
            let charVC = CharacterViewController(model: model.character(atIndex: 0, forAffiliation: .galacticEmpire))
            
            // LO metemos en otro Navigation
            let charNav = UINavigationController(rootViewController: charVC)
            
            // Creamos un SplitView y le endosamos los dos navs
            let splitVC = UISplitViewController()
            splitVC.viewControllers = [uNav, charNav]
            
            // Split como root View controller
            window?.rootViewController = splitVC
            
            
            // Asignamos delegados
            uVC.delegate = charVC
            
            
            // Mostramos la window
            window?.makeKeyAndVisible()
            
            return true
            
        }catch{
            fatalError("Error while loading JSON")
        }
        
     }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

