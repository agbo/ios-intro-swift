//
//  CharacterViewController.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 6/27/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import UIKit

class CharacterViewController: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var photoView: UIImageView!
    var model : StarWarsCharacter
    
    //MARK: - Initialization
    init(model: StarWarsCharacter){
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func syncModelWithView(){
        
        // Photo
        photoView.image = model.photo
        
        // Alias
        title = model.name
        
    }
    
    //MARK: - Actions
    @IBAction func displayWiki(sender: AnyObject) {
        
        // Crear un WikiVC
        let wVC = WikiViewController(model: model)
        
        // Hacer un push sobre mi NavigationController
        navigationController?.pushViewController(wVC, animated: true)
        
        
    }
    @IBAction func playSound(sender: AnyObject) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Antes de que la vista tenga sus dimensiones
        // Una sola vez
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        // Para que el VC se ajste al espacio que deja
        // el navigation
        // edgesForExtendedLayout = .None
        
        // Justo antes de montrarse (después de viewDidLoad)
        // Posiblemente más de una vez
        syncModelWithView()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
}



extension CharacterViewController: UniverseViewControllerDelegate{
    
    func universeViewController(vc: UniverseViewController, didSelectCharacter character: StarWarsCharacter) {
        
        
        // Actualizar el modelo
        model = character
        
        // Sincronizar las vistas con el nuevo modelo
        syncModelWithView()
        
    }
}





















