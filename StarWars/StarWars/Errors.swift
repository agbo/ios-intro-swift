//
//  Errors.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 6/27/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import Foundation

// MARK: JSON Errors
enum StarWarsError : ErrorType{
    case wrongURLFormatForJSONResource
    case resourcePointedByURLNotReachable
    case jsonParsingError
    case wrongJSONFormat
    case nilJSONObject
}