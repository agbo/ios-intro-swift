//
//  StarWarsUniverse.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 6/23/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import UIKit


class StarWarsUniverse {
    
    
    //MARK: Utility types
    typealias StarWarsArray         =   [StarWarsCharacter]
    typealias StarWarsDictionary    =   [StarWarsAffiliation : StarWarsArray]
    
    
    
    //MARK: - Properties
    var dict : StarWarsDictionary = StarWarsDictionary()
    
    
    init(characters chars : StarWarsArray){
        
        // Crear un diccionario vacio
        dict = makeEmptyAffiliations()
        
        
        // Nos pateamos el array en bruto y asignamos
        // según afiliación
        for each in chars{
            
            dict[each.affiliation]?.append(each)
            
        }
        
        
    }
    
    var affiliationCount : Int{
        get{
            // indicar cuantas afiliaciones hay
            return dict.count
        }
    }
    
    
    func characterCount(forAffiliation affiliation: StarWarsAffiliation)-> Int{
        
        // cuantos personajes hay para esta afiliación?
        guard let count = dict[affiliation]?.count else{
            return 0
        }
        return count
        
        
       
    }
    
    func character(atIndex index: Int,
                forAffiliation affiliation: StarWarsAffiliation) -> StarWarsCharacter{
        
        // el personaje nº index en la afiliación affiliation
        let chars = dict[affiliation]!
        let char = chars[index]
        
        return char
    
    }
    
    func affiliationName(affiliation: StarWarsAffiliation) -> String{
        
        return affiliation.rawValue
    }
    
    //MARK: Utils
    func makeEmptyAffiliations() -> StarWarsDictionary {
        
        var d = StarWarsDictionary()
        
        d[.rebelAlliance]       =   StarWarsArray()
        d[.galacticEmpire]      =   StarWarsArray()
        d[.firstOrder]          =   StarWarsArray()
        d[.jabbaCriminalEmpire] =   StarWarsArray()
        d[.unknown]             =   StarWarsArray()
        
        return d
    }
}







